<?php
declare(strict_types=1);

namespace App\Fixtures;

use App\Entity\Employee;
use App\Entity\Export;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;

class FillInDatabaseFixture extends Fixture
{
    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $data = json_decode(file_get_contents(__DIR__ . '/fixtureData.json'));
        foreach ($data as $item) {
            $room = new Room($item->room);
            $manager->persist($room);
            $manager->flush();

            $employee = new Employee($item->employee->firstName, $item->employee->lastName);
            $manager->persist($employee);
            $manager->flush();

            $export = new Export();
            $export->setName($item->name);
            $export->setEmployee($employee);
            $export->setRoom($room);
            $export->setCreatedAt(new \DateTimeImmutable($item->datetime));
            $manager->persist($export);
            $manager->flush();
        }
        $manager->flush();
    }
}
