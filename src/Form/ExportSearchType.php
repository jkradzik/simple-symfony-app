<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Export;
use App\Entity\Room;
use App\Repository\RoomRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExportSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startDate', DateType::class, [
                "label" => "Data od",
                'widget' => 'single_text',
                'required' => false,
                'mapped' => false,
            ])
            ->add('endDate', DateType::class, [
                "label" => "Data do",
                'widget' => 'single_text',
                'required' => false,
                'mapped' => false,

            ])
            ->add('room', EntityType::class, [
                'class' => Room::class,
                'query_builder' => function (RoomRepository $roomRepository) {
                    return $roomRepository->createQueryBuilder('r');
                },
                'choice_label' => function (Room $room) {
                    return sprintf('%s (%s)', $room->getName(), $room->getId());
                },
                'label' => "Pomieszczenie",
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Szukaj"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Export::class]);
    }
}
