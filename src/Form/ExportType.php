<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Employee;
use App\Entity\Export;
use App\Entity\Room;
use App\Repository\EmployeeRepository;
use App\Repository\RoomRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                "label" => "Nazwa eksportu"
            ])
            ->add('createdAt', DateTimeType::class, [
                "label" => "Data utworzenia",
                "widget" => "single_text",
                "disabled" => true
            ])
            ->add('employee', EntityType::class, [
                'class' => Employee::class,
                'query_builder' => function (EmployeeRepository $employeeRepository) {
                    return $employeeRepository->createQueryBuilder('e');
                },
                'choice_label' => function (Employee $employee) {
                    return sprintf('%s %s', $employee->getFirstName(), $employee->getLastName());
                }
            ])
            ->add('room', EntityType::class, [
                'class' => Room::class,
                'query_builder' => function (RoomRepository $roomRepository) {
                    return $roomRepository->createQueryBuilder('r');
                },
                'choice_label' => function (Room $room) {
                    return sprintf('%s (%s)', $room->getName(), $room->getId());
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Export::class]);
    }
}
