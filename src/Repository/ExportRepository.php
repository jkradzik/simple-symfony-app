<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Export;
use App\Entity\Room;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Export>
 *
 * @method Export|null find($id, $lockMode = null, $lockVersion = null)
 * @method Export|null findOneBy(array $criteria, array $orderBy = null)
 * @method Export[]    findAll()
 * @method Export[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Export::class);
    }

    public function add(Export $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Export $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByRoomAndDateRange(?Room $room, ?DateTime $startDate, ?DateTime $endDate)
    {
        $qb = $this->createQueryBuilder('e');

        if ($room) {
            $qb->andWhere('e.room = :val')->setParameter('val', $room);
        }

        if ($startDate) {
            $qb->andWhere('e.createdAt >= :startDate')->setParameter('startDate', $startDate);
        }

        if ($endDate) {
            $qb->andWhere('e.createdAt <= :endDate')->setParameter('endDate', $endDate);
        }

        return $qb->orderBy('e.createdAt', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
