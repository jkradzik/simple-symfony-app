<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoomRepository::class)]
class Room
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name;

    #[ORM\OneToMany(mappedBy: 'room', targetEntity: Export::class)]
    private Collection $export;

    public function __construct(?string $name = null)
    {
        $this->export = new ArrayCollection();
        $this->name = $name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Export>
     */
    public function getExport(): Collection
    {
        return $this->export;
    }

    public function addExport(Export $export): self
    {
        if (!$this->export->contains($export)) {
            $this->export->add($export);
            $export->setRoom($this);
        }

        return $this;
    }

    public function removeExport(Export $export): self
    {
        if ($this->export->removeElement($export)) {
            // set the owning side to null (unless already changed)
            if ($export->getRoom() === $this) {
                $export->setRoom(null);
            }
        }

        return $this;
    }
}
