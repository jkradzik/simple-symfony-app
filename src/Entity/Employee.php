<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
class Employee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private readonly int $id;

    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: Export::class)]
    private Collection $export;

    #[ORM\Column(length: 255)]
    private string $firstName;

    #[ORM\Column(length: 255)]
    private string $lastName;

    public function __construct(?string $firstName = null, ?string $lastName = null)
    {
        $this->export = new ArrayCollection();
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Export>
     */
    public function getExport(): Collection
    {
        return $this->export;
    }

    public function addExport(Export $export): self
    {
        if (!$this->export->contains($export)) {
            $this->export->add($export);
            $export->setEmployee($this);
        }

        return $this;
    }

    public function removeExport(Export $export): self
    {
        if ($this->export->removeElement($export)) {
            // set the owning side to null (unless already changed)
            if ($export->getEmployee() === $this) {
                $export->setEmployee(null);
            }
        }

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}
