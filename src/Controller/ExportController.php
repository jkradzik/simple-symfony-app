<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Export;
use App\Form\ExportSearchType;
use App\Form\ExportType;
use App\Repository\ExportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/export')]
class ExportController extends AbstractController
{
    #[Route('/', name: 'app_export_index', methods: ['GET', 'POST'])]
    public function index(
        Request $request,
        ExportRepository $exportRepository): Response
    {
        $form = $this->createForm(ExportSearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exports = $exportRepository->findByRoomAndDateRange(
                $form['room']->getData(),
                $form['startDate']->getData(),
                $form['endDate']->getData()
            );
        } else {
            $exports = $exportRepository->findAll();
        }

        return $this->renderForm('export/index.html.twig', [
            'exports' => $exports,
            'form' => $form,
        ]);
    }

    #[Route('/new', name: 'app_export_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ExportRepository $exportRepository): Response
    {
        $export = new Export();
        $form = $this->createForm(ExportType::class, $export);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exportRepository->add($export, true);

            return $this->redirectToRoute('app_export_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('export/new.html.twig', [
            'export' => $export,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_export_show', methods: ['GET'])]
    public function show(Export $export): Response
    {
        return $this->render('export/show.html.twig', [
            'export' => $export,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_export_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Export $export, ExportRepository $exportRepository): Response
    {
        $form = $this->createForm(ExportType::class, $export);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exportRepository->add($export, true);

            return $this->redirectToRoute('app_export_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('export/edit.html.twig', [
            'export' => $export,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_export_delete', methods: ['POST'])]
    public function delete(Request $request, Export $export, ExportRepository $exportRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $export->getId(), $request->request->get('_token'))) {
            $exportRepository->remove($export, true);
        }

        return $this->redirectToRoute('app_export_index', [], Response::HTTP_SEE_OTHER);
    }
}
