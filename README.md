<h2 align="center">Simple Symfony Application</h3>

## O projekcie

<p>Aplikacja wykonana w stylu MVC na kontynerze przy pomocy Dockera.
Oparta na najnowszym aktualnie frameworku Symfony (6) i PHP w wersji 8.1.
Poza tym do realizacji użyto jeszcze: Doctrine (+migrations), Twig, Webpack, Bootstrap, Symfony
Fixtures, Symfony Form Component, CS-FIXER etc.</p>
<hr>
<p>Jakubek Software Jakub Radzik | a.d. 2022</p>

Zrzuty ekranu:

* ![O aplikacji](assets/files/ssa1.png)
* ![Lista eksportów](assets/files/ssa2.png)
* ![Lista pracowników](assets/files/ssa3.png)
* ![Dane eksportu](assets/files/ssa4.png)
* ![Edycja eksportu](assets/files/ssa5.png)
