compose=docker-compose -f docker-compose.yml
s=app

export compose

.PHONY: start
start: up

.PHONY: up
up:
	$(compose) up -d --build --force-recreate
	@echo 'Uruchamiam simple symfony app'
	@echo 'APP -> http://localhost:2215/'
	@echo 'DB -> http://localhost:2216/'

.PHONY: stop
stop:
	$(compose) rm -v -f
	$(compose) stop

.PHONY: sh
sh:
	$(compose) exec $(s) bash

.PHONY: logs
logs:
	$(compose) logs -f $(s)
